﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//First Example
public class TransformExample : MonoBehaviour
{
    void Update()
    {
        transform.Translate(new Vector3(0,0,1));
    }
}


//Second Example
public class TransformExample : MonoBehaviour {

	public float speed = 10f;


	void Update ()
	{
		transform.Translate(Vector3.forward, speed * Time.deltaTime);
	}
}